# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression.
Use the insertion algorithm"""

import sys

# Ordered list of image formats, from lower to higher insertion
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """

    if fordered.index(format2) < fordered.index(format1):
        return False
    else:
        return True

def sort_pivot(formats: list, pivot: int):
    """Sort the pivot format, by exchanging it with the format
    on its left, until it gets ordered.
    """

    for x in range(pivot, len(formats)):
        actual = x
        while (actual > 0):
            if lower_than(formats[actual-1], formats[actual]) is False:
                formats[actual-1], formats[actual] = formats[actual], formats[actual-1]
            actual = actual-1

    return formats

def sort_formats(formats: list) -> list:
    """Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered. Use the insertion algorithm"""

    for pivot in (1,len(formats)):
        sort_pivot(formats, pivot)
    return formats



def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""
    sum2=0
    formats: list = sys.argv[1:]

    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")

    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        sum2 += 1
        if sum2 == len(sorted_formats):
            print(format)
        else:
            print(format, end=" ")





if __name__ == '__main__':
    main()
